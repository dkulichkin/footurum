'use strict';

var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
	skill_id: { type: mongoose.Schema.Types.ObjectId, required: true, index: true, ref: 'Skill' },
	name: {
		en: { type: String },
		ru: { type: String },
		ua: { type: String }
	},
	description: {
		en: {type: String },
		ru: {type: String },
		ua: {type: String }
	},
	image: {type: String },
	practical: {type: Boolean, default: false},
	weeks_total: { type: Number },
	tpw: {type: Number, default: 1},
	dpt: {type: Number, default: 60},
	order: {type: Number, default: 0}
}, { versionKey: false });

module.exports = mongoose.model('Foot', Schema, 'fr_foots');
