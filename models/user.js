'use strict';

var crypto = require('crypto');
var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
	first_name: { type: String, required: true },
	last_name: { type: String, required: true },
	image: { type: String },
	email: { type: String, required: true, index: true },
	language: { type: String, default: 'en' },
	facebook: {
		id: { type: String },
		token: { type: String }
	},
	twitter: {
		id: { type: String },
		token: { type: String },
		username: { type: String }
	},
	password: { type: String, set: function(password)
		{
			this.salt = this.makeSalt();
			return this.encryptPassword(password);
		}
	},
	role: { type: String, default: 'user' },
	salt: { type: String },
	skills: [
		{
			_id : false,
			skill_id: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Skill' },
			name: { type: String, required: true },
			image: { type: String },
			language: { type: String, required: true },
			foots_completed: [
				{ type: mongoose.Schema.Types.ObjectId, ref: 'Foot' }
			],
			signed_at: { type: Date, required: true }
		}
	],
	organizer_actions: [
		{
			foot_id: { type: mongoose.Schema.Types.ObjectId, required: true },
			skill_id: { type: mongoose.Schema.Types.ObjectId, required: true },
			name: { type: String, required: true },
			dpt: { type: Number, required: true },
			date: { type: Date, required: true },
			notification: { type: Number, default: 0 }
		}
	]
}, { versionKey: false });

Schema.methods.authenticate = function(password)
{
	return this.encryptPassword(password) === this.password;
};

Schema.methods.encryptPassword = function(password)
{
	if (!password || !this.salt) return '';
	var salt = new Buffer(this.salt, 'base64');
	return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
};

Schema.methods.makeSalt = function()
{
	return crypto.randomBytes(16).toString('base64');
};

module.exports = mongoose.model('User', Schema, 'fr_users');


