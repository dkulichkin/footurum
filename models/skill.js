'use strict';

var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
	category_id: { type: mongoose.Schema.Types.ObjectId, required: true, index: true, ref: 'SkillCategory' },
	active: { type: Boolean, default: false },
	languages: { type: [String], required: true },
	name: {
		en: { type: String },
		ru: { type: String },
		ua: { type: String }
	},
	description: {
		en: { type: String },
		ru: { type: String },
		ua: { type: String }
	},
	image: { type: String },
	budget: { type: String },
	num_followers: { type: Number },
	duration_total: { type: Number },
	tags: {
		en: [{ type: String }],
		ru: [{ type: String }],
		ua: [{ type: String }]
	},
	goals: [
		{
			en: { type: String },
			ru: { type: String },
			ua: { type: String }
		}
	],
	expert_advices: [
		{
			en: { type: String },
			ru: { type: String },
			ua: { type: String }
		}
	],
	foot_groups: [
		{
			group_name: {
				en: { type: String },
				ru: { type: String },
				ua: { type: String }
			},
			foots: [
				{type: [mongoose.Schema.Types.ObjectId], ref: 'Foot'}
			]
		}
	]
}, { versionKey: false });

Schema.index(
	{
		"name.en": 'text', "name.ru": 'text', "name.ua": 'text',
		"tags.en": 'text', "tags.ru": 'text', "tags.ua": 'text',
		"description.en": 'text', "description.ru": 'text', "description.ua": 'text'
	},
	{
		name: 'text_index',
		weights: {
			"name.en": 10, "name.ru": 10, "name.ua": 10,
			"tags.en": 8, "tags.ru": 8, "tags.ua": 8,
			"description.en": 5, "description.ru": 5, "description.ua": 5
		}
	}
);

module.exports = mongoose.model('Skill', Schema, 'fr_skills');
