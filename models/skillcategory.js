'use strict';

var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
	name: {
		en: {type: String, required: true},
		ru: {type: String, required: true},
		ua: {type: String, required: true}
	}
}, { versionKey: false });

module.exports = mongoose.model('SkillCategory', Schema, 'fr_skillcategories');
