'use strict';

var express = require('express');
var router = express.Router();
var UserModel = require('../../models').User;

// Passport Configuration
require('./local/passport').setup(UserModel);

router.use('/local', require('./local'));

module.exports = router;
