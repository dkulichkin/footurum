'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../../../components/auth-service');

var router = express.Router();

router.post('/', function(req, res)
{
  return passport.authenticate('local', function(err, user, info)
  {
    err = err || info;
    if (err) return res.status(401).json(err);
    var token = auth.signToken(user._id, user.role, user.language);
	  return res.json({token: token}).end();
  })(req, res);
});

module.exports = router;
