'use strict';

var i18n = require('i18n');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

module.exports.setup = function(User)
{
	passport.use(
		new LocalStrategy({
		  usernameField: 'email',
		  passwordField: 'password'
		},
		function(email, password, done)
		{
			return User.findOneAsync(
				{ email: email },
				{ _id: 1, role: 1, language: 1, password: 1, salt: 1 }
			)
				.then(function(user)
				{
					return !user || !user.authenticate(password)
						? done(null, null, { error: i18n.__('Profile is not found or credentials are incorrect') })
						: done(null, user);
				})
				.catch(function(err)
				{
					return done(err);
				});
		}
	));
};
