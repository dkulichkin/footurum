'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../components/auth-service');
var router = express.Router();

router.get('/:id', auth.hasRole('user'), controller.detail);
router.post('/:id/complete', auth.hasRole('user'), controller.complete);
router.post('/:id/resume', auth.hasRole('user'), controller.resume);

module.exports = router;
