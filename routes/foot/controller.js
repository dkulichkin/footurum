/**
 * Foot Controller
 */

'use strict';

var debug = require('../../components/debug');
var handleMissedParamError = debug.handleMissedParamError;
var handleControllerError = debug.handleControllerError;

var _ = require('lodash');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var FootModel = require('../../models/').Foot;
var UserModel = require('../../models/').User;
var UserController = require('../user/controller');

/**
 * Shows foot details
 * access level: @user
 */
exports.detail = function(req, res, next)
{
	var result = {},
			foot_id = req.params.id
	;

	if (!mongoose.Types.ObjectId.isValid(foot_id)) return handleMissedParamError('id', next);

	return Promise.all([
		UserModel.findOneAsync(
			{ _id: new mongoose.Types.ObjectId(req.user.id) },
			{ _id: 0, "skills": 1 }
		),
		FootModel.findOneAsync({ _id: new mongoose.Types.ObjectId(foot_id) })
	])
		.spread(function(user, foot)
		{
			if (!user) throw new Error('Token points to non-exisintg user');

			if (!foot)
			{
				var err = new Error('Foot is not found');
				err.http_code = 404;
				throw err;
			}

			var skill = _.find(user.skills, { skill_id: foot.skill_id }),
					language = skill ? skill.language : req.user.language
			;

			result = foot.toObject();

			// Finalizing result
			if (req.user.role === 'user')
			{
				result.name = result.name[language];
				result.description = result.description[language];
			}

			return res.json(result).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * Marks foot as completed
 * access level: @user
 */
exports.complete = function(req, res, next)
{
	var foot_id = req.params.id;

	if (!mongoose.Types.ObjectId.isValid(foot_id)) return handleMissedParamError('id', next);

	return Promise.all([
		UserModel.findOneAsync(
			{ _id: new mongoose.Types.ObjectId(req.user.id) },
			{ "skills": 1 }
		),
		FootModel.findOneAsync({ _id: new mongoose.Types.ObjectId(foot_id) })
	])
		.spread(function(user, foot)
		{
			var userFootsCompleted = [], skill, err;

			if (!user) throw new Error('Token points to non-exisintg user');

			if (!foot)
			{
				err = new Error('Foot is not found');
				err.http_code = 404;
				throw err;
			}

			_.pluck(user.skills, 'foots_completed').forEach(function(foots_completed)
			{
				userFootsCompleted = userFootsCompleted.concat(foots_completed.map(function(foot) { return foot.toString() }));
			});
			if (userFootsCompleted.indexOf(foot_id) !== -1)
			{
				err = new Error('Foot is already completed');
				err.http_code = 400;
				throw err;
			}

			skill = _.find(user.skills, { skill_id: foot.skill_id });
			if (!skill)
			{
				err = new Error('Foot does not belong to subscribed skills');
				err.http_code = 400;
				throw err;
			}

			skill.foots_completed.push(new mongoose.Types.ObjectId(foot_id));
			user.markModified('skills');
			skill.markModified('foots_completed');

			return user.save();
		})
		.then(function()
		{
			return UserController.me(req, res, next);
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});

};

/**
 * Marks foot as resumed back
 * access level: @user
 */
exports.resume = function(req, res, next)
{
	var foot_id = req.params.id;

	if (!mongoose.Types.ObjectId.isValid(foot_id)) return handleMissedParamError('id', next);

	return Promise.all([
		UserModel.findOneAsync(
			{ _id: new mongoose.Types.ObjectId(req.user.id) },
			{ "skills": 1 }
		),
		FootModel.findOneAsync({ _id: new mongoose.Types.ObjectId(foot_id) })
	])
		.spread(function(user, foot)
		{
			var userFootsCompleted = [], skill, err;

			if (!user) throw new Error('Token points to non-exisintg user');

			if (!foot)
			{
				err = new Error('Foot is not found');
				err.http_code = 404;
				throw err;
			}

			_.pluck(user.skills, 'foots_completed').forEach(function(foots_completed)
			{
				userFootsCompleted = userFootsCompleted.concat(foots_completed.map(function(foot) { return foot.toString() }));
			});
			if (userFootsCompleted.indexOf(foot_id) === -1)
			{
				err = new Error('Foot is already in progress');
				err.http_code = 400;
				throw err;
			}

			skill = _.find(user.skills, { skill_id: foot.skill_id });
			if (!skill)
			{
				err = new Error('Foot does not belong to subscribed skills');
				err.http_code = 400;
				throw err;
			}

			_.remove(skill.foots_completed, function(id) { return id.toString() === foot_id });
			user.markModified('skills');
			skill.markModified('foots_completed');

			return user.save();
		})
		.then(function()
		{
			return UserController.me(req, res, next);
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});

};
