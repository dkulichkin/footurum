'use strict';

var debug = require('../../components/debug');
var handleMissedParamError = debug.handleMissedParamError;
var handleControllerError = debug.handleControllerError;

var _ = require('lodash');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var SkillCategoryModel = require('../../models/').SkillCategory;
var SkillModel = require('../../models/').Skill;
var UserModel = require('../../models/').User;

/**
 * Get list of categories
 * access level: @user
 */
exports.index = function(req, res, next)
{
	var result,
			limit = process.env.SKILLS_CATSCROLL_PER_TIME
	;

	return Promise.all([
		SkillCategoryModel.findAsync(),
		UserModel.findOneAsync(
			{ _id: new mongoose.Types.ObjectId(req.user.id) },
			{ _id: 0, skills: 1, "skills.skill_id": 1 }
		)
	])
		.spread(function(categoryList, user)
		{
			if (!user) throw new Error('Token points to non-exisintg user');

			var signedSkillIds = _.pluck(user.skills, 'skill_id');

			result = categoryList;

			return Promise.all(categoryList.map(function(category)
			{
				return SkillModel.find
					(
						{ _id: { $nin: signedSkillIds }, languages: req.user.language, active: true, category_id: new mongoose.Types.ObjectId(category._id) },
						{ _id: 1, name: 1, description: 1, image: 1, tags: 1, num_followers: '$num_followers', category_id: 1 }
					)
					.limit(limit).sort({'num_followers': -1})
					.execAsync();
			}));
		})

		.then(function(skillsByCategory)
		{
			var categoryIdToSkillsMap = {};

			skillsByCategory.forEach(function(skills)
			{
				if (skills.length === 0) return;

				var category_id = skills[0].category_id;

				categoryIdToSkillsMap[category_id] = skills.map(function(skill)
				{
					return {
						_id: skill._id,
						name: skill.name[req.user.language],
						tags: skill.tags ? skill.tags[req.user.language] : [],
						description: skill.description[req.user.language],
						num_followers: skill.num_followers,
						image: skill.image
					}
				});
			});

			result = result.map(function(category)
			{
				category = category.toObject();
				category.name = category.name[req.user.language];
				category.skills = categoryIdToSkillsMap[category._id];
				return category;
			});

			return res.json(result).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * Get a single category with its skills
 * access level: @user
 */
exports.detail = function(req, res, next)
{
	var result = {}, query,
			category_id = req.params.id,
			offset = req.query.offset || 0,
			limit = req.query.count || process.env.SKILLS_CATSCROLL_PER_TIME
	;

	if (!mongoose.Types.ObjectId.isValid(category_id)) return handleMissedParamError('id', next);

	return UserModel.findOneAsync(
		{ _id: new mongoose.Types.ObjectId(req.user.id) },
		{ _id: 0, skills: 1, "skills.skill_id": 1 }
	)
		.then(function(user)
		{
			if (!user) throw new Error('Token points to non-exisintg user');

			var signedSkillIds = _.pluck(user.skills, 'skill_id');

			query = { _id: { $nin: signedSkillIds }, languages: req.user.language, active: true, category_id: new mongoose.Types.ObjectId(category_id) };

			return SkillModel
				.find(query, { _id: 1, name: 1, description: 1, image: 1, tags: 1, num_followers: '$num_followers' })
				.skip(offset).limit(limit).sort({'num_followers': -1})
				.execAsync();
		})
		.then(function(skills)
		{
			result._id = category_id;
			result.skills = skills.map(function(skill)
			{
				return {
					_id: skill._id,
					name: skill.name[req.user.language],
					tags: skill.tags ? skill.tags[req.user.language] : [],
					description: skill.description[req.user.language],
					num_followers: skill.num_followers,
					image: skill.image
				}
			});

			return SkillModel.countAsync(query);
		})
		.then(function(size)
		{
			result.size = size;
			return res.json(result).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};
