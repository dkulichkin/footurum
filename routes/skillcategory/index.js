'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../components/auth-service');
var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.detail);

module.exports = router;
