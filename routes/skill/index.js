'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../components/auth-service');
var router = express.Router();

router.get('/tags/:tag', auth.isAuthenticated(), controller.tags);
router.get('/search', auth.hasRole('user'), controller.search);
router.get('/:id', auth.hasRole('user'), controller.detail);
router.post('/:id/subscribe', auth.hasRole('user'), controller.subscribe);
router.post('/:id/unsubscribe', auth.hasRole('user'), controller.unsubscribe);

module.exports = router;
