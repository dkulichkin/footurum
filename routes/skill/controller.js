/**
 * Skill Controller
 */

'use strict';

var debug = require('../../components/debug');
var handleMissedParamError = debug.handleMissedParamError;
var handleControllerError = debug.handleControllerError;

var _ = require('lodash');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var SkillModel = require('../../models/').Skill;
var FootModel = require('../../models/').Foot;
var UserModel = require('../../models/').User;
var UserController = require('../user/controller');

/**
 * Shows skill and its foots
 * access level: @user
 */
exports.detail = function(req, res, next)
{
	var result = {},
			skill_id = req.params.id
	;

	if (!mongoose.Types.ObjectId.isValid(skill_id)) return handleMissedParamError('id', next);

	return Promise.all([
		SkillModel.findOneAsync({ _id: new mongoose.Types.ObjectId(skill_id) }),
		FootModel.findAsync({ skill_id: new mongoose.Types.ObjectId(skill_id) }, {_id: 1, name: 1, image: 1})
	])
		.spread(function(skill, foots)
		{
			if (!skill)
			{
				var err = new Error('Skill is not found');
				err.http_code = 404;
				throw err;
			}

			var footIdMap = {},
					skillLanguage = req.user.language
			;

			result = skill.toObject();

			// Prepairng foots
			foots.forEach(function(foot)
			{
				foot = foot.toObject();

				// Localizing foot's name and description
				if (req.user.role === 'user')
				{
					foot.name = foot.name[skillLanguage];
				}

				footIdMap[foot._id] = foot;
				return foot;
			});

			// Finalizing result
			if (req.user.role === 'user')
			{
				result.name = result.name[skillLanguage];
				result.description = result.description[skillLanguage];
				result.tags = result.tags[skillLanguage];
				result.goals = result.goals.map(function(goal)
				{
					return goal[skillLanguage];
				});
				result.expert_advices = result.expert_advices.map(function(advice)
				{
					return advice[skillLanguage];
				});
			}

			result.foot_groups = result.foot_groups.map(function(foot_group)
			{
				// Localizing group's name
				if (req.user.role === 'user')
				{
					foot_group.group_name = foot_group.group_name[skillLanguage];
				}

				foot_group.foots = foot_group.foots.map(function(foot_id)
				{
					return footIdMap[foot_id];
				});

				return foot_group;
			});

			return res.json(result).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * Shows skills by search term
 * access level: @user
 */
exports.search = function(req, res, next)
{
	var result = {}, query,
			offset = req.query.offset || 0,
			limit = req.query.count || process.env.SKILLS_CATSCROLL_PER_TIME
	;

	if (!req.query.q) return handleMissedParamError('q', next);

	return UserModel.findOneAsync(
		{ _id: new mongoose.Types.ObjectId(req.user.id) },
		{ _id: 0, skills: 1, "skills.skill_id": 1 }
	)
		.then(function(user)
		{
			if (!user) throw new Error('Token points to non-exisintg user');

			var signedSkillIds = _.pluck(user.skills, 'skill_id');

			query = { _id: { $nin: signedSkillIds }, active: true, languages: req.user.language,  $text : { $search : req.query.q } };

			return SkillModel.find(query, { score : { $meta: "textScore" } })
				.sort({ score : { $meta : 'textScore' } })
				.skip(offset).limit(limit)
				.execAsync();

		})
		.then(function(searchResults)
		{
			result.skills = searchResults.map(function(skill)
			{
				return {
					_id: skill._id,
					name: skill.name[req.user.language],
					tags: skill.tags ? skill.tags[req.user.language] : [],
					description: skill.description[req.user.language],
					num_followers: skill.num_followers,
					image: skill.image
				}
			});

			result.skills = result.skills.sort(function(a,b)
			{
				return b.num_followers - a.num_followers;
			});

			return SkillModel.countAsync(query);
		})
		.then(function(size)
		{
			result.size = size;
			return res.json(result).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * Get skills by tag
 * access level: @user
 */
exports.tags = function(req, res, next)
{
	var result = {}, query,
			offset = req.query.offset || 0,
			limit = req.query.count || process.env.SKILLS_CATSCROLL_PER_TIME
	;

	if (!req.params.tag) return handleMissedParamError('tag', next);

	return UserModel.findOneAsync(
		{ _id: new mongoose.Types.ObjectId(req.user.id) },
		{ _id: 0, skills: 1, "skills.skill_id": 1 }
	)
		.then(function(user)
		{
			if (!user) throw new Error('Token points to non-exisintg user');

			var signedSkillIds = _.pluck(user.skills, 'skill_id');

			query = {
				_id: { $nin: signedSkillIds },
				languages: req.user.language,
				active: true,
				$or: [{"tags.en": req.params.tag}, {"tags.ru": req.params.tag}, {"tags.ua": req.params.tag}] }
			;

			return SkillModel.find(query, { _id: 1, name: 1, description: 1, image: 1, tags: 1, num_followers: '$num_followers' })
				.skip(offset).limit(limit).sort({'num_followers': -1})
				.execAsync();
		})
		.then(function(skills)
		{
			result.skills = skills.map(function(skill)
			{
				return {
					_id: skill._id,
					name: skill.name[req.user.language],
					tags: skill.tags ? skill.tags[req.user.language] : [],
					description: skill.description[req.user.language],
					num_followers: skill.num_followers,
					image: skill.image
				}
			});

			return SkillModel.countAsync(query);
		})
		.then(function(size)
		{
			result.size = size;
			return res.json(result).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * Subscribe to skill
 * access level: @user
 */
exports.subscribe = function(req, res, next)
{
	var skill_id = req.params.id;

	if (!mongoose.Types.ObjectId.isValid(skill_id)) return handleMissedParamError('id', next);

	return Promise.all([
		UserModel.findOneAsync(
			{ _id: new mongoose.Types.ObjectId(req.user.id) },
			{ "skills": 1 }
		),
		SkillModel.findOneAsync({ _id: new mongoose.Types.ObjectId(skill_id), active: true })
	])
		.spread(function(user, skill)
		{
			if (!user) throw new Error('Token points to non-exisintg user');

			if (!skill)
			{
				var err = new Error('Skill is not found');
				err.http_code = 404;
				throw err;
			}

			var signedSkillIds = _.pluck(user.skills, 'skill_id').map(function(id) { return id.toString() });

			if (signedSkillIds.indexOf(skill_id) !== -1)
			{
				var err = new Error('Skill is already subscribed');
				err.http_code = 400;
				throw err;
			}

			user.skills.push({
				skill_id: new mongoose.Types.ObjectId(skill_id),
				language: req.user.language,
				signed_at: new Date(),
				name: skill.name[req.user.language],
				image: skill.image
			});

			user.markModified('skills');

			return Promise.all([
				user.save(),
				SkillModel.updateAsync({_id: new mongoose.Types.ObjectId(skill_id)}, { $inc: {num_followers: 1} })
			]);
		})
		.then(function()
		{
			return UserController.me(req, res, next);
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * Unsubsribe from skill
 * access level: @user
 */
exports.unsubscribe = function(req, res, next)
{
	var skill_id = req.params.id;

	if (!mongoose.Types.ObjectId.isValid(skill_id)) return handleMissedParamError('id', next);

	return Promise.all([
		UserModel.findOneAsync(
			{ _id: new mongoose.Types.ObjectId(req.user.id) },
			{ "skills": 1, "organizer_actions": 1 }
		),
		SkillModel.findOneAsync({ _id: new mongoose.Types.ObjectId(skill_id), active: true })
	])
		.spread(function(user, skill)
		{
			if (!user) throw new Error('Token points to non-exisintg user');

			if (!skill)
			{
				var err = new Error('Skill is not found');
				err.http_code = 404;
				throw err;
			}

			var signedSkillIds = _.pluck(user.skills, 'skill_id').map(function(id) { return id.toString() });

			if (signedSkillIds.indexOf(skill_id) === -1)
			{
				var err = new Error('Skill is not subscribed');
				err.http_code = 400;
				throw err;
			}

			_.remove(user.skills, {skill_id: new mongoose.Types.ObjectId(skill_id)});
			_.remove(user.organizer_actions, {skill_id: new mongoose.Types.ObjectId(skill_id)});

			user.markModified('skills');
			user.markModified('organizer_actions');

			return Promise.all([
				user.save(),
				SkillModel.updateAsync({_id: new mongoose.Types.ObjectId(skill_id)}, { $inc: {num_followers: -1} })
			]);
		})
		.then(function()
		{
			return UserController.me(req, res, next);
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};
