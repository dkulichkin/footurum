'use strict';

var express = require('express');
var controller = require('./controller');
var auth = require('../../components/auth-service');
var router = express.Router();

router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/me/organizer', auth.isAuthenticated(), controller.addToOrganizer);
router.delete('/me/organizer/:id', auth.isAuthenticated(), controller.removeFromOrganizer);
router.get('/me/organizer', auth.isAuthenticated(), controller.organizer);
router.get('/:id', auth.hasRole('admin'), controller.detail);
router.post('/', auth.hasRole('admin'), controller.create);
router.delete('/:id', auth.hasRole('admin'), controller.delete);

module.exports = router;
