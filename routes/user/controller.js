/**
 * User Controller
 */

'use strict';

var debug = require('../../components/debug');
var handleMissedParamError = debug.handleMissedParamError;
var handleControllerError = debug.handleControllerError;

var _ = require('lodash');
var moment = require('moment');
var mongoose = require('mongoose');
var UserModel = require('../../models/').User;
var FootModel = require('../../models/').Foot;

/**
 * Creates a new user
 * access level: @admin
 */
exports.create = function(req, res, next)
{

};

/**
 * Get a single user
 * access level: @admin
 */
exports.detail = function(req, res, next)
{
	var result;
	var id = req.params.id;

	if (!mongoose.Types.ObjectId.isValid(id)) return handleMissedParamError('id', next);

	return UserModel.aggregateAsync(
		{ $match: {_id: new mongoose.Types.ObjectId(id) } },
		{ $unwind: '$organizer_actions'},
		{
			$match: { $and : [
				{ "organizer_actions.date": { $gte: moment().startOf('day').toDate() }},
				{ "organizer_actions.date": { $lte: moment().add('1', 'week').endOf('week').add(1, 'day').toDate() }}
			]}
		},
		{ $sort: { "organizer_actions.date": 1 } },
		{
			$group: {
				_id: { first_name: '$first_name', last_name: '$last_name', email: '$email', language: '$language', skills: '$skills', image: '$image' },
				organizer_actions: {$push: '$organizer_actions' }
			}
		}
	)
		.then(function(user)
		{
			var response;
			if (user && user[0])
			{
				response = user[0]._id;
				response.organizer_actions = user[0].organizer_actions;
			}
			else
			{
				response = UserModel.findOneAsync(
					{ _id: new mongoose.Types.ObjectId(id) },
					{ _id: 0, first_name: 1, last_name: 1, email: 1, language: 1, skills: 1, image: 1, organizer_actions: 1 }
				);
			}
			return response;
		})
		.then(function(user)
		{
			var skill_ids;

			if (!user)
			{
				var err = new Error('User is not found');
				err.http_code = 404;
				throw err;
			}

			result = user;

			if (!result.skills || !result.skills.length) return;

			skill_ids = result.skills.map(function(skill) { return new mongoose.Types.ObjectId(skill.skill_id) });

			// Calculating progress on skill
			return FootModel.aggregateAsync(
				{ $match: { skill_id: { $in: skill_ids } } },
				{ $group: { _id: '$skill_id', foots: { $push: { foot_id: '$_id', practical: '$practical'} } } }
			)
				.then(function(skills)
				{
					var skillsData = {};

					skills.forEach(function(skill)
					{
						var footIdsToPractical = {}, theoretical_count = 0, practical_count = 0, footWeight;

						skill.foots.forEach(function(foot)
						{
							footIdsToPractical[foot.foot_id] = foot.practical;
							foot.practical ? practical_count++ : theoretical_count++;
						});

						footWeight = +Number(100 / (4 * practical_count + theoretical_count)).toFixed(2);

						skillsData[skill._id] = {
							footIdsToPractical: footIdsToPractical,
							theory_weight: footWeight,
							practice_weight: 4 * footWeight
						};
					});

					result.skills.forEach(function(skill)
					{
						var progress = 0;
						var skillData = skillsData[skill.skill_id];
						skill.foots_completed.forEach(function(foot_id)
						{
							progress += skillData.footIdsToPractical[foot_id] ? +skillData.practice_weight : +skillData.theory_weight;
						});
						skill.progress = Math.ceil(progress);
					});
				});

		})
		.then(function()
		{
			return res.json(result).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * Get my organizer items by timestamps
 * access level: @user
 */
exports.organizer = function(req, res, next)
{
	var response;

	if (!req.query.from || (new Date(+req.query.from)).toString() === 'Invalid Date') return handleMissedParamError('from', next);

	if (!req.query.to || (new Date(+req.query.to)).toString() === 'Invalid Date') return handleMissedParamError('to', next);

	return UserModel.aggregateAsync(
		{ $match: {_id: new mongoose.Types.ObjectId(req.user.id) } },
		{ $unwind: '$organizer_actions'},
		{
			$match: { $and : [
				{ "organizer_actions.date": { $gte: new Date(+req.query.from) }},
				{ "organizer_actions.date": { $lte: new Date(+req.query.to) }}
			]}
		},
		{ $sort: { "organizer_actions.date": 1 } },
		{
			$group: {
				_id: { skills: '$skills' },
				organizer_actions: {$push: '$organizer_actions' }
			}
		}
	)
		.then(function(user)
		{
			response = user && user[0] ? user[0].organizer_actions : [];
			return res.json(response).end();
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * adds action to the organizer
 * access level: @user
 */
exports.addToOrganizer = function(req, res, next)
{
	var reqValidationPromises = [],
			action, actions = req.body.actions,
			err, actionsSkillIds = []
	;

	if (!actions || !actions.length) return handleMissedParamError('actions', next);

	else
	{
		for (var i = 0, l = actions.length; i < l; i++)
		{
			action =  req.body.actions[i];

			if (!mongoose.Types.ObjectId.isValid(action.foot_id)) return handleMissedParamError('actions => foot_id', next);

			if (!mongoose.Types.ObjectId.isValid(action.skill_id)) return handleMissedParamError('actions => skill_id', next);

			if (!action.name) return handleMissedParamError('actions => name', next);

			if (!action.dpt) return handleMissedParamError('actions => dpt', next);

			if (!action.date || (new Date(+action.date)).toString() === 'Invalid Date') return handleMissedParamError('actions => date', next);

			actionsSkillIds.push(action.skill_id);
			action.foot_id = new mongoose.Types.ObjectId(action.foot_id);
			action.skill_id = new mongoose.Types.ObjectId(action.skill_id);

			reqValidationPromises.push(FootModel.findOneAsync({
				_id: action.foot_id,
				skill_id: action.skill_id
			}));
		}
	}

	Promise.all(reqValidationPromises)
		.then(function(foots)
		{
			if (_.filter(foots, function(foot) { return foot !== null }).length !== actions.length)
			{
				err = new Error('Mismatching of foots and skills in request');
				err.http_code = 400;
				throw err;
			}

			return UserModel.findOneAsync(
				{ _id: new mongoose.Types.ObjectId(req.user.id) },
				{ organizer_actions: 1, skills: 1 }
			);
		})
		.then(function(user)
		{
			if (!user)
			{
				err = new Error('User is not found');
				err.http_code = 404;
				throw err;
			}

			var userSkillIds = user.skills.map(function(skill) { return skill.skill_id.toString() });
			for (var i = 0, l = actionsSkillIds.length; i < l; i++)
			{
				if (userSkillIds.indexOf(actionsSkillIds[i]) === -1)
				{
					err = new Error('Trying to add action for a non-subscribed skill');
					err.http_code = 404;
					throw err;
				}
			}

			actions.forEach(function(action)
			{
				user.organizer_actions.push(action);
			});

			user.markModified('organizer_actions');

			return user.save();
		})
		.then(function()
		{
			return exports.me(req, res, next);
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});

};

/**
 * removes action form the organizer
 * access level: @user
 */
exports.removeFromOrganizer = function(req, res, next)
{
	var id = req.params.id;

	if (!mongoose.Types.ObjectId.isValid(id)) return handleMissedParamError('id', next);

	return UserModel.updateAsync(
		{ _id: new mongoose.Types.ObjectId(req.user.id) },
		{ $pull: { organizer_actions: { _id: new mongoose.Types.ObjectId(id) } } }
	)
		.then(function()
		{
			return exports.me(req, res, next);
		})
		.catch(function(err)
		{
			return handleControllerError(err, req, next);
		});
};

/**
 * removes user
 * access level: @admin
 */
exports.delete = function(req, res, next)
{

};

/**
 * Get my info
 * access level: @user
 */
exports.me = function(req, res, next)
{
	req.params.id = req.user.id;
	return exports.detail(req, res, next);
};
