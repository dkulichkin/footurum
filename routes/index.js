'use strict';

var debug = require('../components/debug');
var reportFunctionDefault = debug.app.error;

module.exports = function(app)
{
	// API routes
	app.use('/users', require('./user'));
	app.use('/skillcategories', require('./skillcategory'));
	app.use('/skills', require('./skill'));
	app.use('/foots', require('./foot'));
	app.use('/signin', require('./signin'));

	// The edge general-purpose Express response error handler and logger
	app.use('/*', function(err, req, res, next)
	{
		if (!err) return next();

		var reportFunction = err.reportFunction || reportFunctionDefault;

		res.status(err.http_code || 500).json({error: err.message}).end();

		// If we have a real message behind - substitute it
		err.message = err.original_message || err.message;

		return reportFunction(err, req);
	});

	// All the rest are redirected to 404
	app.route('/*').all(function(req, res)
	{
		return res.status(404).end();
	});
};
