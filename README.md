# Authorization

Get a jwt token

```
POST /signin/local
{
    "email": "user@footurum.com",
    "password": "12345"
}
```

Success response:

```
200

{
  "token": "eyJ0eXAiOiJKV1QiLC ... ph28"
}
```

Wrong credentials:

```
401

{
  "message": "Profile is not found or credentials are incorrect"
}
```

Token expired (currently 120 days):

```
401

{
  "message": "Authorization expired, please re-enter your credentials"
}
```

For all non-authorized requests further when user's role is not enough for accessing the resource:

```
403

{
  "message": "You've not enough rights for accessing this resource"
}
```

Afterwards, send the authorization header with every request:
 
```
Authorization: Bearer eyJ0eXAiOiJKV1QiLC ... ph28
```

Here and after - all invalid parameter requests are with the response:

```
400

{
  "error": "Missed parameter X or has a wrong format"
}
```

# User's profile

Retrieving:

```
GET /users/me
```

Response, the organizer is sorted by date and grabbed for the current and the next weeks:

```
200

{
  "first_name": "User",
  "last_name": "Footurum",
  "email": "user@footurum.com",
  "language": "en",
  "skills": [
    {
      "skill_id": "5609a7d613ed49a938a7ffcb",
      "name": "Become a marathon runner",
      "language": "en",
      "signed_at": "2015-09-28T20:49:26.929Z",
      "progress": 0,
      "foots_completed": [
        "5609a7d613ed49a938a7fff9"
      ],
      "is_done": false
    },
    ...
  ],
  "organizer_actions": [
    {
      "foot_id": "5609a7d713ed49a938a80023",
      "skill_id": "5609a7d613ed49a938a7ffef",
      "name": "Консультация со специалистом по требованию",
      "dpt": 60,
      "date": "2015-09-29T08:00:00.097Z",
      "_id": "5609a7d713ed49a938a8002a",
      "notification": 0
    },
    ...
  ]
}
```

Using wrong token for accessing own profile - 404

Retrieving of organizer actions by time scope:

```
GET /users/me/organizer?from={timestamp}&to={timestamp}
```

Response:

```
200

[
  {
    "foot_id": "5609ad6ce945f86f40b29b41",
    "skill_id": "5609ad6be945f86f40b29b10",
    "name": "Упражнения по контролю гнева",
    "dpt": 20,
    "date": "2015-09-28T08:00:00.194Z",
    "_id": "5609ad6ce945f86f40b29b4c",
    "notification": 0
  },
  {
    "foot_id": "5609ad6ce945f86f40b29b40",
    "skill_id": "5609ad6be945f86f40b29b10",
    "name": "Изучить причины возникновения гнева",
    "dpt": 60,
    "date": "2015-09-28T10:00:00.199Z",
    "_id": "5609ad6ce945f86f40b29b50",
    "notification": 0
  }
]
```

Adding actions to organizer
  
```
PUT /users/me/organizer
{
    "actions":[
        {
          "foot_id": "561032e814bf584fa1883c0d",
          "skill_id": "561032e814bf584fa1883bde",
          "name": "123",
          "dpt": 120,
          "date": 1443898944136
        },
        {
          "foot_id": "561032e814bf584fa1883c0d",
          "skill_id": "561032e814bf584fa1883bde",
          "name": "345",
          "dpt": 90,
          "date": 1443899944136
        }
    ]
}
```

Response: updated user's profile, similarly to skill's subscribing


Removing of item from the organizer:

```
DELETE /users/me/organizer/561032e814bf584fa1883bde
```

Response: updated user's profile, similarly to skill's subscribing


# Category of skills

Getting all categories with their top5 by followers most popular skills

```
GET /skillcategories
```

Response:

```
200

[
  {
    "_id": "5609ad6be945f86f40b29af3",
    "name": "Physical",
    "skills": [
      {
        "_id": "5609ad6be945f86f40b29af3",
        "name": "Become a yacht skipper",
        "tags": [
          "sport",
          "lifestyle",
          "hobby"
        ],
        "description": "Basic recommended steps",
        "num_followers": 100
      },
      ...
    ]
  },
  {
    "_id": "5609ad6be945f86f40b29ae9",
    "name": "Emotional"
  },
  ...
]
```

Getting all skills inside category by its id, by the number of count (default to 5) per time, ordered by number of followers:

```
GET /skillcategories/5609ad6be945f86f40b29af3?offset={integer}&count={integer}
```

Response:

```
200

{
  "_id": "5609ad6be945f86f40b29ae8",
  "skills": [
    {
      "_id": "5609ad6be945f86f40b29af3",
      "name": "Become a yacht skipper",
      "tags": [
        "sport",
        "lifestyle",
        "hobby"
      ],
      "description": "Basic recommended steps ",
      "num_followers": 100
    },
    ...
  ],
  "size": 6
}
```

# Skill

Retrieving of skill details by id:

```
GET /skills/5609ad6be945f86f40b29ae8
```

Response:

```
200

{
  "_id": "5609ad6be945f86f40b29af3",
  "category_id": "5609ad6be945f86f40b29ae8",
  "budget": "$1000-3000",
  "duration_total": 12,
  "num_followers": 100,
  "foot_groups": [
    {
      "_id": "5609ad6ce945f86f40b29b25",
      "foots": [
        {
          "_id": "5609ad6ce945f86f40b29b20",
          "name": "Sail sport basic theory"
        },
        {
          "_id": "5609ad6ce945f86f40b29b21",
          "name": "Navigation theory"
        }
      ],
      "group_name": "Week 1st"
    },
    {
      "_id": "5609ad6ce945f86f40b29b24",
      "foots": [
        {
          "_id": "5609ad6ce945f86f40b29b22",
          "name": "Practice on schwertboot"
        },
        {
          "_id": "5609ad6ce945f86f40b29b23",
          "name": "Practice on yacht"
        }
      ],
      "group_name": "Week 2nd"
    }
  ],
  "expert_advices": [
    "Loren ipsum dolor sit amet",
    "Loren ipsum dolor sit amet",
    "Loren ipsum dolor sit amet"
  ],
  "goals": [
    "Acquaint with main yacht types and their construction parts",
    "Familiarise with basics of navifation",
    "Get tips on buying the boat"
  ],
  "tags": ["sport", "lifestyle", "hobby"],
  "description": "Basic recommended steps for becoming a skipper",
  "name": "Become a yacht skipper",
  "languages": ["en", "ua", "ru"]
}
```

Search skills by a keyword (if count is omitted - default is set to 5):

```
GET /skills/search?q={term}&offset={integer}&count={integer}
```

Response, the offset is handled similarly to category's detail listing:

```
200

{
  "skills": [
    {
      "_id": "5609ad6be945f86f40b29af3",
      "name": "Become a yacht skipper",
      "tags": [
        "sport",
        "lifestyle",
        "hobby"
      ],
      "description": "Basic recommended steps in aquainting with a theory",
      "num_followers": 100
    },
   ...
  ],
  "size": 6
}
```

Subscribing to skill

```
POST /skills/5609ad6be945f86f40b29af3/subscribe
```

Response - an updated complete profile of the user without an organizer with the skill been added:

```
200

{
  "first_name": "User",
  "last_name": "Footurum",
  "email": "user@footurum.com",
  "language": "en",
  "skills": [
    {
      "skill_id": "5609a7d613ed49a938a7ffcb",
      "name": "Become a marathon runner",
      "language": "en",
      "signed_at": "2015-09-28T20:49:26.929Z",
      "progress": 0,
      "foots_completed": ["5609a7d613ed49a938a7fff9"],
      "is_done": false
    },
    ...
  ],
  "organizer_actions": [
      {
        "foot_id": "5609a7d713ed49a938a80023",
        "skill_id": "5609a7d613ed49a938a7ffef",
        "name": "Консультация со специалистом по требованию",
        "dpt": 60,
        "date": "2015-09-29T08:00:00.097Z",
        "_id": "5609a7d713ed49a938a8002a",
        "notification": 0
      },
      ...
    ]
}
```

Unsubscribing from skill

```
POST /skills/5609ad6be945f86f40b29af3/unsubscribe
```

Response: updated user's profile, similarly to skill's subscribing


Getting skills by a tag keyword:

```
GET /skills/tags/sport?offset={integer}&count={integer}
```

Response - see previous category detail listing for example

# Foot

Retrieving the certain foot's details by its id:

```
GET /foots/5609ad6be945f86f40b29af3
```

Response:

```
{
  "_id": "5609ad6ce945f86f40b29b41",
  "skill_id": "5609ad6be945f86f40b29b10",
  "weeks_total": 2,
  "order": 2,
  "dpt": 20,
  "tpw": 3,
  "practical": true,
  "description": "я - сообщение, практика методики 6 секунд",
  "name": "Упражнения по контролю гнева"
}
```

Marking foot as completed

```
POST /foots/5609ad6be945f86f40b29af3/complete
```

Response: updated user's profile, similarly to skill's subscribing

Marking foot as completed

```
POST /foots/5609ad6be945f86f40b29af3/resume
```

Response: updated user's profile, similarly to skill's subscribing
