'use strict';

require('dotenv').load();
require('sprintf.js');
require('../models/');

var debug = require('../components/debug');
var log = debug.app.log;
var error = debug.app.error;

var env = process.env.NODE_ENV;
var cluster = require("cluster");
var numCPUs = require("os").cpus().length;

if (cluster.isMaster)
{
	// Connecting to db and seeding with an apropriate strategy
	var connection = require('../components/mongo-connection');
	var seedStrategy = (env === 'development' || process.env.SEEDING_FROM_SCRATCH) ? 'development' : 'production';

	log('app started on ' + env + ' environment');

	require('../components/seed/' + seedStrategy)
		.then(function()
		{
			return connection.close();
		});

	for (var i = 0; i < numCPUs; i++)
	{
		cluster.fork();
	}

	cluster
		.on("exit", function(worker)
		{
			error(worker.process.pid + ' died');
			cluster.fork();
		})

		.on('online', function(worker) {
			log("Worker " + worker.process.pid + " has been spinned");
		});

}
else
{
	require('./www');
}