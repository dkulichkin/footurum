'use strict';

require('dotenv').load();
require('../components/mongo-connection');

var env = process.env.NODE_ENV;
var cluster = require('cluster');
var identity = cluster.isMaster ? 'master' : 'worker';
var i18n = require('i18n');
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var passport = require('passport');
var rollbar = require('rollbar');

var debug = require('../components/debug');
var log = debug.app.log;
var error = debug.app.error;

i18n.configure({
	locales:['en', 'ru', 'ua'],
	directory: __dirname + '/../locales'
});

// default: using 'accept-language' header to guess language settings
app.use(i18n.init);

app.use(compression({level:9}));
app.use(passport.initialize());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '20mb'}));
app.use(methodOverride());

// Body parser error handler
app.use(function(err, req, res, next)
{
	return err ? res.status(400).end() : next();
});

// Rollbar express errors handler
if (env !== 'alpha' && env !== 'development') app.use(rollbar.errorHandler(process.env.ROLLBAR_KEY));

process.on('uncaughtException', error);

require('../routes/')(app);

function BootEnv(callback)
{
	//Start server
	server.listen(process.env.SERVER_PORT, function(err)
	{
		err ? error(err) : log('listening on port ' + process.env.SERVER_PORT + " from " + identity + " " + process.pid);

		if (callback) return callback();
	});
}

if (process.env.NODE_ENV === 'test')
{
	before(function(done)
	{
		BootEnv(done);
	});

	after(function()
	{
		server.close();
	});
}
else
{
	BootEnv();
}
