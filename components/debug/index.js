'use strict';

var debug = require('debug');
var rollbar = require("rollbar");
var logError = function(type)
{
	return process.env.NODE_ENV === 'development'|| process.env.NODE_ENV === 'alpha'
		? function(e) { debug(type).call(null, e) }
		: function(e, request) { rollbar.handleError.apply(null, arguments) }
};

rollbar.init(process.env.ROLLBAR_KEY, {
	environment: process.env.NODE_ENV,
	verbose: false
});

module.exports = {
	app: {
		log: debug('app'),
		error: logError('app:error')
	},
	database: {
		log: debug('database'),
		error: logError('database:error')
	},
	controller: {
		log: debug('controller'),
		error: logError('controller:error')
	},
	handleMissedParamError: function(param, next)
	{
		var err = new Error(sprintf('Missed parameter %s or has a wrong format', param));
		err.http_code = 400;
		err.reportFunction = module.exports.controller.error;
		return next(err);
	},
	handleControllerError: function(err, req, next)
	{
		err.original_message = err.message;
		err.message = req.__('Server error');
		err.reportFunction = module.exports.controller.error;
		return next(err);
	}
};
