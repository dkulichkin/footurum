'use strict';

var debug = require('../debug/');
var log = debug.database.log;
var error = debug.database.error;

var cluster = require('cluster');
var identity = cluster.isMaster ? 'master' : 'worker';
var Promise = require('bluebird');
var mongoose = Promise.promisifyAll(require('mongoose'));
var mongoUri = sprintf('mongodb://%s:%d/%s', process.env.MONGO_HOST, process.env.MONGO_PORT, process.env.MONGO_DB_NAME);

mongoose.connection
	.on('error', error)
	.on('close', log.bind(this, 'mongo connection is closed from ' + identity + ' ' + process.pid))
	.once('open', log.bind(this, 'mongodb connected from ' + identity + ' ' + process.pid));

mongoose.connect(mongoUri);

module.exports = mongoose.connection;