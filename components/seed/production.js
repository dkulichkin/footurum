'use strict';

var debug = require('../debug');
var log = debug.database.log;
var error = debug.database.error;

var Promise = require('bluebird');
var mongoose = require('mongoose');
var models = require('../../models/');
var UserModel = models.User;
var SkillCategoryModel = models.SkillCategory;

module.exports =  Promise.all([
	UserModel.findOneAsync({role: 'admin'}),
	UserModel.findOneAsync({role: 'user'}),
	SkillCategoryModel.findAsync()
])
	.spread(function(admin, user, categories)
	{
		var seedingDeferreds = [];
		if (!admin)
		{
			seedingDeferreds.push(new UserModel({email: 'admin@footurum.com', first_name: 'Admin', last_name: 'Footurum', password: process.env.SEEDING_ADMIN_PASSWORD, role: 'admin'}).save());
		}
		if (!user)
		{
			seedingDeferreds.push(new UserModel({email: 'user@footurum.com', first_name: 'User', last_name: 'Footurum', password: process.env.SEEDING_USER_PASSWORD, role: 'user'}).save());
		}
		if (categories.length === 0)
		{
			seedingDeferreds.push(Promise.all([
				new SkillCategoryModel({name: {en: 'Physical', ru: 'Физические', ua: 'Фізичні'} }).save(),
				new SkillCategoryModel({name: {en: 'Emotional', ru: 'Эмоциональные', ua: 'Емоційні'} }).save(),
				new SkillCategoryModel({name: {en: 'Intellectual', ru: 'Интеллектуальные', ua: 'Інтеллектуальні'} }).save(),
				new SkillCategoryModel({name: {en: 'Mental', ru: 'Ментальные', ua: 'Ментальні'} }).save()
			]));
		}

		if (seedingDeferreds.length) log('start seeding production data');

		return Promise.all(seedingDeferreds);
	});
