'use strict';

var debug = require('../debug');
var log = debug.database.log;
var error = debug.database.error;

var _ = require('lodash');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var moment = require('moment');
var models = require('../../models/');
var UserModel = models.User;
var SkillCategoryModel = models.SkillCategory;
var SkillModel = models.Skill;
var FootModel = models.Foot;

var adminModelInstance, userModelInstance,
		skillModelInstances = {},
		footModelInstancesWeekFirst = [], footModelInstancesWeekSecond = []
;

log('start seeding development data');

// Deleting an old db stuff
module.exports = Promise.all([
	UserModel.removeAsync(),
	SkillCategoryModel.removeAsync(),
	SkillModel.removeAsync(),
	FootModel.removeAsync()
])

	// Users
	.then(function()
	{
		return Promise.all([
			new UserModel({email: 'admin@footurum.com', first_name: 'Admin', last_name: 'Footurum', password: process.env.SEEDING_ADMIN_PASSWORD, role: 'admin'}).save(),
			new UserModel({email: 'user@footurum.com', first_name: 'User', last_name: 'Footurum', password: process.env.SEEDING_USER_PASSWORD, role: 'user'}).save()
		]);
	})

	// Skill categories
	.spread(function(admin, user)
	{
		adminModelInstance = admin;
		userModelInstance = user;
		userModelInstance.skills = [];

		return Promise.all([
			new SkillCategoryModel({name: {en: 'Physical', ru: 'Физические', ua: 'Фізичні'} }).save(),
			new SkillCategoryModel({name: {en: 'Emotional', ru: 'Эмоциональные', ua: 'Емоційні'} }).save(),
			new SkillCategoryModel({name: {en: 'Intellectual', ru: 'Интеллектуальные', ua: 'Інтеллектуальні'} }).save(),
			new SkillCategoryModel({name: {en: 'Mental', ru: 'Ментальные', ua: 'Ментальні'} }).save()
		]);
	})

	// Skills
	.spread(function(physical, emotional, intellectual, mental)
	{
		return Promise.all([
			(new SkillModel({
				category_id: new mongoose.Types.ObjectId(physical._id),
				active: true,
				name: {en: 'Become a marathon runner', ru: 'Стать марафонским бегуном', ua: 'Стати марафонським бігуном'},
				description: {en: 'During the course you will prepare your body, learn theory and practice for personal trainings, get useful tips for participation in amateur competitions.', ru: 'В течении курса вы подготовите организм, освоите теорию и практику персональных треннировок, получите полезные советы по подготовке к соревнованиям аматорского уровня.', ua: 'На протязі курсу ви підготуєте організм, опануєте теорію і практику персональних тренувань, отрімаєте корисні поради щодо підготовки до змагань амаортського рівня.'},
				languages: ['en', 'ua', 'ru'],
				duration_total: 12,
				num_followers: 10,
				tags: {
					en: ['sport', 'lifestyle', 'hobby'],
					ru: ['спорт', 'стильжизни', 'хобби'],
					ua: ['спорт', 'стильжиття', 'хоббі']
				},
				expert_advices: [
					{ en: 'Loren ipsum dolor sit amet', ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' },
					{ en: 'Loren ipsum dolor sit amet', ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' },
					{ en: 'Loren ipsum dolor sit amet', ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' }
				],
				goals: [
					{ en: 'Get basic knowledge of the marathon running skill', ru: 'Овладеть базовыми заниями о марафонском беге', ua: 'Здобути базові знання про марафонський біг'},
					{ en: 'Effectively develop the skill practically within the shortest period of time', ru: 'Эффективно практически овладеть навыком в кратчайшее время.', ua: 'Ефективно практично опанувати навичку у найкоротший проміжок часу.'},
					{ en: 'Prepare to an annual city running competition', ru: 'Подготовиться к ежегодным городским соревнованиям', ua: 'Підготуватися до щорічних міських змагань'}
				]
			})).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(physical._id),
				active: true,
				budget: '$1000-3000',
				name: {en: 'Become a yacht skipper', ru: 'Стать яхтенным капитаном', ua: 'Стати яхтенним капітаном'},
				description: {en: 'Basic recommended steps in aquainting with a theory and practice of the sail sport', ru: 'Базовые рекомендуемые шаги в овладении теорией и практикой парусного спорта', ua: 'Базові рекомендовані кроки в опануванні теорією та практикою вітрильного спорту'},
				languages: ['en', 'ua', 'ru'],
				duration_total: 12,
				num_followers: 100,
				tags: {
					en: ['sport', 'lifestyle', 'hobby'],
					ru: ['спорт', 'стильжизни', 'хобби'],
					ua: ['спорт', 'стильжиття', 'хоббі']
				},
				expert_advices: [
					{ en: 'Loren ipsum dolor sit amet', ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' },
					{ en: 'Loren ipsum dolor sit amet', ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' },
					{ en: 'Loren ipsum dolor sit amet', ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' }
				],
				goals: [
					{ en: 'Acquaint with main yacht types and their construction parts', ru: 'Познакомиться с основными типами яхт и их устройством', ua: 'Ознайомитися з основними типами яхт та їх конструкцією'},
					{ en: 'Familiarise with basics of navifation', ru: 'Познакомиться с основами навигации', ua: 'Ознайомитися з основами навігації'},
					{ en: 'Get tips on buying the boat', ru: 'Получить советы о покупке своей лодки', ua: 'Отримати поради щодо купівлі власного човна'}
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(physical._id),
				active: true,
				name: {en: 'Intro to triathlon'},
				description: {en: 'A versatile and comprehensive roadmap guide for a triathlon practice'},
				languages: ['en'],
				duration_total: 6,
				num_followers: 50,
				tags: {
					en: ['sport', 'lifestyle', 'hobby']
				},
				goals: [
					{ en: 'Become a triathlon beginner' }
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(physical._id),
				active: true,
				name: {en: 'Become a good swimmer'},
				description: {en: 'A versatile and comprehensive roadmap guide for a swimming practice'},
				languages: ['en'],
				duration_total: 6,
				num_followers: 10,
				tags: {
					en: ['sport', 'lifestyle', 'hobby']
				},
				goals: [
					{ en: 'Master and boost your swimming practice' }
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(physical._id),
				active: true,
				name: {en: 'Boxing workouts'},
				description: {en: 'First steps in acquainting with a boxing sport'},
				languages: ['en'],
				duration_total: 6,
				num_followers: 5,
				tags: {
					en: ['sport', 'lifestyle', 'hobby']
				},
				goals: [
					{ en: 'Master and boost your boxing practice' }
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(physical._id),
				active: true,
				name: {en: 'Basketball workouts'},
				description: {en: 'First steps in acquainting with a basketball sport'},
				languages: ['en'],
				duration_total: 5,
				num_followers: 23,
				tags: {
					en: ['sport', 'hobby', 'teamgames']
				},
				goals: [
					{ en: 'Learn how to play basketball' },
					{ en: 'Participate in amateur championships' }
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(physical._id),
				active: true,
				name: {en: 'Beginner\'s roadmap to playing football'},
				description: {en: 'First steps in acquainting with a football sport'},
				languages: ['en'],
				duration_total: 5,
				num_followers: 2,
				tags: {
					en: ['sport', 'hobby', 'teamgames']
				},
				goals: [
					{ en: 'Learn how to play football' },
					{ en: 'Participate in amateur championships' }
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(intellectual._id),
				active: true,
				budget: 'Depends...',
				name: {en: 'Art of negotiation', ru: 'Искусство переговоров', ua: 'Мистецтво перемовин'},
				description: {en: 'In the modern world the art of negotiation became to be a valuable skill almost in all aspects of our life...', ru: 'В современном мире искусство переговоров уже давно стало важным навыком практически во всех сверах деятельности...', ua: 'У сучасному світі мистецтво перемовин вже давно стало важливою навичкою практично у всіх сферах діяльності...'},
				languages: ['en', 'ua', 'ru'],
				num_followers: 10,
				duration_total: 2,
				tags: {
					en: ['society', 'career', 'business'],
					ru: ['социум', 'карьера', 'бизнес'],
					ua: ['соціум', 'кар\'єра', 'бізнес']
				},
				goals: [
					{ en: 'Conduct negotiations inside your working team', ru: 'Проводить переговоры с командой сотрудников', ua: 'Проводити перемовини у команді співробітників'},
					{ en: 'To be able successfully finish interpersonal conflicts on daily issues', ru: 'Быть способным успешно разрешать межличностные конфликты в повседневных делах', ua: 'Бути спроможнім вирішувати міжособісти конфлікти у повсякденних справах'}
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(intellectual._id),
				active: true,
				name: { ru: 'Когнитивные навыки', ua: 'Когнітивні навички'},
				description: { ru: 'Предлагаемый план развития знакомит с базовыми техниками улучшения когнитивных навыков и интеллектуальной производительности', ua: 'Пропонований план розвитку знайомить із базовими техніками покращення когнитівних навичок та інтеллектупльної продуктивності.'},
				languages: ['ua', 'ru'],
				num_followers: 2,
				duration_total: 2,
				tags: {
					ua: ['інтеллект', 'продуктивність'],
					ru: ['интеллект', 'продуктивность']
				},
				expert_advices: [
					{ ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' },
					{ ru: 'Loren ipsum dolor sit amet', ua: 'Loren ipsum dolor sit amet' },
				],
				goals: [
					{ ru: 'Улучшить память', ua: 'Покращити пам\'ять'},
					{ ru: 'Улучшить скорость внимания', ua: 'Покращити швидкість уваги'}
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(intellectual._id),
				active: false,
				name: {en: 'Basics of calculus and math analysis', ru: 'Основы дифференциального исчисления и мат-анализа', ua: 'Основи диференційного обчисленння та мат-аналізу'},
				description: {en: 'A comprehensive guide for the subject', ru: 'Подробное и всестороннее руководство по предмету', ua: 'Докладне та всеохоплююче роз\'яснення предмету'},
				languages: ['en', 'ua', 'ru'],
				duration_total: 2,
				tags: {
					en: ['math', 'science'],
					ru: ['математика', 'наука'],
					ua: ['математика', 'наука']
				},
				goals: [
					{ en: 'Learn how to determine derivatives and integrals', ru: 'Научиться вычислять производные и интеграллы', ua: 'Навчитися розраховувати похідні та інтеграли'}
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(emotional._id),
				active: true,
				languages: ['ru'],
				tags: {
					ru: ['социум', 'карьера', 'привычки']
				},
				name: {ru: 'Умение управлять своими чуствами и эмоциями (ГНЕВ)'},
				description: {ru: 'Умение направленное на контроль и управления чувством гнева - эмоцией, которая возникает в результате раздражения, угрозы, препятствий, нарушения кем-либо жизненных границ'},
				duration_total: 2,
				num_followers: 10,
				goals: [
					{ ru: 'научиться управлять гневом' },
					{ ru: 'научиться справляться с чрезмерной раздражительностью' },
					{ ru: 'контроль над яростью (драки, рукоприкладство, потеря контроля над собой, состояние аффекта)' },
					{ ru: 'научиться не выплескивать свой гнев на других людей' },
					{ ru: 'научиться выражать гнев безопасным способом' },
					{ ru: 'научиться осозновать и защищать свои личные границы' },
					{ ru: 'жить без агрессии и насилия' }
				]
			}).save(),

			new SkillModel({
				category_id: new mongoose.Types.ObjectId(mental._id),
				active: true,
				languages: ['en', 'ru'],
				budget: '20 usd',
				num_followers: 10,
				tags: {
					ru: ['социум', 'карьера', 'привычки'],
					en: ['society', 'career', 'habits']
				},
				name: {en: 'Willpower improvement', ru: 'Как развить и укрепить силу воли'},
				description: {en: 'The Willpower Instinct: How Self-Control Works, Why It Matters, and What You Can Do to Get More of It', ru: 'Сила воли: Как работает самоконтроль; Почему именно так; И что еще можно сделать, что улучшить результат'},
				goals: [
					{en: 'To improve willpower', ru: 'Развить и укрепить силу воли'}
				]
			}).save()

		]);
	})

	.spread(function(
		marathonRunnerSkill,
		sailSportSkill,
		triathlonSkill,
		swimmingSkill,
		boxingSkill,
		basketballSkill,
		footballSkill,
		negotiationsSkill,
		cognitiveSkill,
		calculusSkill,
		angerMediationSkill,
		willpowerImprovementSkill
	)
	{
		skillModelInstances = {
			marathonRunnerSkill: marathonRunnerSkill,
			sailSportSkill: sailSportSkill,
			triathlonSkill: triathlonSkill,
			swimmingSkill: swimmingSkill,
			boxingSkill: boxingSkill,
			basketballSkill: basketballSkill,
			footballSkill: footballSkill,
			negotiationsSkill: negotiationsSkill,
			cognitiveSkill: cognitiveSkill,
			calculusSkill: calculusSkill,
			angerMediationSkill: angerMediationSkill,
			willpowerImprovementSkill: willpowerImprovementSkill
		};
	})

	// Seeding the marathonRunnerSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.marathonRunnerSkill._id),
				name: {en: 'Reading sites on marathon running', ru: 'Ознакомиться с сайтами о марафонском беге', ua: 'Ознайомитися з сайтами про марафонський біг'},
				description: {en: 'For better understanding the subject it\'s always useful to get acquainted with professional sources. We recommend to read the following...' , ru: 'Для лучшего понимания предмета всегда полезно обратиться к профессионалам. Мы рекомендуем следующие ресурсы...', ua: 'Для кращого розуміння предмету завжди корисно ознайомитися з думкою профессіоналів. Ми рекомендуємо наступні ресурси...'},
				practical: false,
				weeks_total: 1,
				tpw: 2,
				dpt: 60,
				order: 1
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.marathonRunnerSkill._id),
				name: {en: 'Reading about the running on different distances', ru: 'Прочитать о беге на различные дистанции', ua: 'Прочитати про біг на різні дистанції'},
				description: {en: 'For better understanding the subject it\'s always useful to get acquainted with professional sources. We recommend to read the following...' , ru: 'Для лучшего понимания предмета всегда полезно обратиться к профессионалам. Мы рекомендуем следующие ресурсы...', ua: 'Для кращого розуміння предмету завжди корисно ознайомитися з думкою профессіоналів. Ми рекомендуємо наступні ресурси...'},
				practical: false,
				weeks_total: 1,
				tpw: 2,
				dpt: 60,
				order: 3
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.marathonRunnerSkill._id),
				name: {en: 'Jogging 2km on everyday basis', ru: 'Ежедневные пробежки 2км', ua: 'Щоденні пробіжки 2км'},
				description: {en: 'First level of everyday jogging workouts: increase the distance by 1 km per week until ready for the next level', ru: 'Первый уровень ежедневных треннировок: уеличивайте дистанцию на 1 км каждую неделю, пока не будете готовыми перейти на следующий уровень', ua: 'Перший рівень щоденних тренувань: збільшуйте відстань по 1км на тиждень до переходу на наступний рівень'},
				weeks_total: 6,
				practical: true,
				tpw: 6,
				dpt: 30,
				order: 2
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.marathonRunnerSkill._id),
				name: {en: 'Jogging 10km on weekly basis', ru: 'Еженедельные пробежки 10км', ua: 'Щотижневі пробіжки 10км'},
				description: {en: 'First level of weekly jogging workouts: increase the distance by 1 km per week until ready for the next level', ru: 'Первый уровень еженедельных треннировок: уеличивайте дистанцию на 1 км каждую неделю, пока не будете готовыми перейти на следующий уровень', ua: 'Перший рівень щотижневих тренувань: збільшуйте відстань по 1км на тиждень до переходу на наступний рівень'},
				weeks_total: 6,
				practical: true,
				tpw: 3,
				dpt: 90,
				order: 2
			}).save()
		]);
	})

	// Creating foot groups for the marathonRunnerSkill with their foots assigned
	// We also add this skill to user as being studied
	.then(function(marathonFoots)
	{
		footModelInstancesWeekFirst = footModelInstancesWeekFirst.concat(marathonFoots.slice(1,3));
		footModelInstancesWeekSecond = footModelInstancesWeekSecond.concat(marathonFoots.slice(3));

		marathonFoots = _.pluck(marathonFoots, '_id');

		skillModelInstances.marathonRunnerSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st', ru: 'Неделя 1', ua: 'Тиждень 1'},
				foots: marathonFoots.slice(1,3)
			},
			{
				group_name: {en: 'Week 2nd', ru: 'Неделя 2', ua: 'Тиждень 2'},
				foots: marathonFoots.slice(3)
			}
		];

		userModelInstance.skills.push({
			skill_id: new mongoose.Types.ObjectId(skillModelInstances.marathonRunnerSkill._id),
			name: skillModelInstances.marathonRunnerSkill.name.en,
			image: skillModelInstances.marathonRunnerSkill.image,
			language: 'en',
			foots_completed: [ new mongoose.Types.ObjectId(marathonFoots[0]) ],
			signed_at: new Date()
		});

		return Promise.all([
			skillModelInstances.marathonRunnerSkill.save(),
			userModelInstance.save()
		]);
	})

	// Seeding the sailSportSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.sailSportSkill._id),
				name: {en: 'Sail sport basic theory', ru: 'Базовая теория парусного спорта', ua: 'Базова теорія вітрильного спорту'},
				description: {en: 'We recommend to start off with the following resources...', ru: 'Мы рекомендуем начать со следующих ресурсов...', ua: 'Ми рекомендуємо почати з наступних ресурсів...'},
				weeks_total: 6,
				practical: false,
				tpw: 3,
				dpt: 60,
				order: 1
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.sailSportSkill._id),
				name: {en: 'Navigation theory', ru: 'Теория навигации', ua: 'Теорія навігації'},
				description: {en: 'We recommend to start off with the following resources...', ru: 'Мы рекомендуем начать со следующих ресурсов...', ua: 'Ми рекомендуємо почати з наступних ресурсів...'},
				weeks_total: 1,
				practical: false,
				tpw: 2,
				dpt: 60,
				order: 3
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.sailSportSkill._id),
				name: {en: 'Practice on schwertboot', ru: 'Практика на швертботе', ua: 'Практика на швертботі'},
				description: {en: 'Attend for classes', ru: 'Запишитесь на секцию', ua: 'Запишіться на секцію'},
				weeks_total: 6,
				practical: true,
				tpw: 1,
				dpt: 180,
				order: 2
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.sailSportSkill._id),
				name: {en: 'Practice on yacht', ru: 'Практика на яхте', ua: 'Практика на яхті'},
				description: {en: 'Attend for classes', ru: 'Запишитесь на секцию', ua: 'Запишіться на секцію'},
				weeks_total: 12,
				practical: true,
				tpw: 1,
				dpt: 240,
				order: 2
			}).save()
		]);
	})

	// Creating foot groups for the sailSportSkill with their foots assigned
	.then(function(sailSportFoots)
	{
		sailSportFoots = _.pluck(sailSportFoots, '_id');

		skillModelInstances.sailSportSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st', ru: 'Неделя 1', ua: 'Тиждень 1'},
				foots: sailSportFoots.slice(0,2)
			},
			{
				group_name: {en: 'Week 2nd', ru: 'Неделя 2', ua: 'Тиждень 2'},
				foots: sailSportFoots.slice(2)
			}
		];

		return skillModelInstances.sailSportSkill.save();
	})

	// Seeding the triathlonSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.triathlonSkill._id),
				name: {en: 'Reading sites on triathlon practice' },
				description: {en: 'To begin it\'s always good to fill in on some background and listen to experts, we recommend the following resources...'},
				practical: false,
				weeks_total: 2,
				tpw: 2,
				dpt: 60,
				order: 0
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.triathlonSkill._id),
				name: {en: 'Jogging 2km on everyday basis'},
				description: {en: 'First level of everyday jogging workouts: increase the distance by 1 km per week until ready for the next level'},
				weeks_total: 6,
				practical: true,
				tpw: 6,
				dpt: 30,
				order: 2
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.triathlonSkill._id),
				name: {en: 'Jogging 10km on weekly basis'},
				description: {en: 'Second level of weekly jogging workouts: increase the distance by 1 km per week until ready for the next level'},
				weeks_total: 6,
				practical: true,
				tpw: 3,
				dpt: 90,
				order: 2
			}).save()
		]);
	})

	// Creating foot groups for the triathlonSkill with their foots assigned
	.then(function(triathlonSkillFoots)
	{
		triathlonSkillFoots = _.pluck(triathlonSkillFoots, '_id');

		skillModelInstances.triathlonSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st'},
				foots: triathlonSkillFoots
			}
		];

		return skillModelInstances.triathlonSkill.save();
	})

	// Seeding the swimmingSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.swimmingSkill._id),
				name: {en: 'Reading sites on swimming practice' },
				description: {en: 'To begin it\'s always good to fill in on some background and listen to experts, we recommend the following resources...'},
				practical: false,
				weeks_total: 2,
				tpw: 2,
				dpt: 60,
				order: 0
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.swimmingSkill._id),
				name: {en: 'Swimming 1km on weekly basis'},
				description: {en: 'Swimming 1km on a weekly basis'},
				weeks_total: 6,
				practical: true,
				tpw: 3,
				dpt: 90,
				order: 2
			}).save()
		]);
	})

	// Creating foot groups for the swimmingSkill with their foots assigned
	.then(function(swimmingSkillFoots)
	{
		swimmingSkillFoots = _.pluck(swimmingSkillFoots, '_id');

		skillModelInstances.swimmingSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st'},
				foots: swimmingSkillFoots
			}
		];

		return skillModelInstances.swimmingSkill.save();
	})

	// Seeding the boxingSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.boxingSkill._id),
				name: {en: 'Reading sites on boxing' },
				description: {en: 'To begin it\'s always good to fill in on some background and listen to experts, we recommend the following resources...'},
				practical: false,
				weeks_total: 2,
				tpw: 2,
				dpt: 60,
				order: 0
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.boxingSkill._id),
				name: {en: 'Boxing class'},
				description: {en: 'Boxing classes'},
				weeks_total: 50,
				practical: true,
				tpw: 2,
				dpt: 120,
				order: 1
			}).save()
		]);
	})

	// Creating foot groups for the boxingSkill with their foots assigned
	.then(function(boxingSkillFoots)
	{
		boxingSkillFoots = _.pluck(boxingSkillFoots, '_id');

		skillModelInstances.boxingSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st'},
				foots: boxingSkillFoots
			}
		];

		return skillModelInstances.swimmingSkill.save();
	})

	// Seeding the basketballSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.basketballSkill._id),
				name: {en: 'Reading sites on basketball' },
				description: {en: 'To begin it\'s always good to fill in on some background and listen to experts, we recommend the following resources...'},
				practical: false,
				weeks_total: 2,
				tpw: 2,
				dpt: 60,
				order: 0
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.basketballSkill._id),
				name: {en: 'Basketball workout'},
				description: {en: 'Basketball workout'},
				weeks_total: 50,
				practical: true,
				tpw: 2,
				dpt: 120,
				order: 1
			}).save()
		]);
	})

	// Creating foot groups for the basketballSkill with their foots assigned
	.then(function(basketballSkillFoots)
	{
		basketballSkillFoots = _.pluck(basketballSkillFoots, '_id');

		skillModelInstances.basketballSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st'},
				foots: basketballSkillFoots
			}
		];

		return skillModelInstances.basketballSkill.save();
	})

	// Seeding the footballSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.footballSkill._id),
				name: {en: 'Reading sites on football' },
				description: {en: 'To begin it\'s always good to fill in on some background and listen to experts, we recommend the following resources...'},
				practical: false,
				weeks_total: 2,
				tpw: 2,
				dpt: 60,
				order: 0
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.footballSkill._id),
				name: {en: 'Football workout'},
				description: {en: 'Football workout'},
				weeks_total: 50,
				practical: true,
				tpw: 2,
				dpt: 120,
				order: 1
			}).save()
		]);
	})

	// Creating foot groups for the footballSkill with their foots assigned
	.then(function(footballSkilllFoots)
	{
		footballSkilllFoots = _.pluck(footballSkilllFoots, '_id');

		skillModelInstances.footballSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st'},
				foots: footballSkilllFoots
			}
		];

		return skillModelInstances.footballSkill.save();
	})

	// Seeding the negotiationsSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.negotiationsSkill._id),
				name: {en: 'Read about basics of negotiation', ru: 'Прочитать про основы переговоров', ua: 'Прочитати про основи переговорів'},
				description: {en: 'Read several books and articles on negotiation tactics and techniques. Look below for the useful articles and books. ', ru: 'Прочесть выдающиеся книги по различным техникам переговоров. Смотрите ниже рекомендованный список книг и статей.', ua: 'Прочитати видатні книжки про різні техніки переговорів. Дивіться нижче рекомендований список книжок та статей.'},
				practical: false,
				weeks_total: 1,
				tpw: 2,
				dpt: 60,
				order: 1
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.negotiationsSkill._id),
				name: {en: 'Video course on basic negotiations', ru: 'Видео-курс по основам переговоров', ua: 'Відео-курс з основ перемовин'},
				description: {en: 'Example', ru: 'Пример', ua: 'Приклад'},
				practical: false,
				weeks_total: 1,
				tpw: 1,
				dpt: 180,
				order: 2
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.negotiationsSkill._id),
				name: {en: 'Conflict resolving practice', ru: 'Практика разрешения конфликта', ua: 'Практика з врегулювання конфлікту'},
				description: {en: 'Choose one of the conflict situations with your nearest colleagues and try to check what is the reason of the conflict and could you mediate it', ru: 'Выберите с коллегами пример конфликтной ситуации из прошлого, попытайтесь определить причину и найдите возможные способы ее решения с учетом полученной информации', ua: 'Оберіть з коллегами приклад конфліктної ситуації з минулого, спробуйте зв\'ясувати причину та знайдіть можливі способи її розв\'язання з урахуванням отриманих знань.'},
				practical: true,
				weeks_total: 3,
				tpw: 1,
				dpt: 30,
				order: 1
			}).save()
		]);
	})

	// Creating foot groups for the negotiationsSkill with their foots assigned
	.then(function(negotiationsSkillFoots)
	{
		negotiationsSkillFoots = _.pluck(negotiationsSkillFoots, '_id');

		skillModelInstances.negotiationsSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st', ru: 'Неделя 1', ua: 'Тиждень 1'},
				foots: negotiationsSkillFoots.slice(0,1)
			},
			{
				group_name: {en: 'Week 2nd', ru: 'Неделя 2', ua: 'Тиждень 2'},
				foots: negotiationsSkillFoots.slice(1)
			}
		];

		return skillModelInstances.negotiationsSkill.save();
	})

	// Seeding the cognitiveSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.cognitiveSkill._id),
				name: {en: 'Cognitive skills training', ru: 'Когнитивные навыки', ua: 'Когнітивні навички'},
				description: {en: 'Use Lumosity service', ru: 'Используйте сервис Lumosity', ua: 'Використайте сервіс Lumosity'},
				practical: true,
				weeks_total: 12,
				tpw: 5,
				dpt: 60,
				order: 1
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.cognitiveSkill._id),
				name: {en: 'Learn more poems', ru: 'Учите стихи', ua: 'Вивчайте вірші'},
				description: {en: 'Learn more poems', ru: 'Учите стихи', ua: 'Вивчайте вірші'},
				practical: true,
				weeks_total: 12,
				tpw: 3,
				dpt: 60,
				order: 2
			}).save()
		]);
	})

	// Creating foot groups for the cognitiveSkill with their foots assigned
	.then(function(cognitiveSkillFoots)
	{
		cognitiveSkillFoots = _.pluck(cognitiveSkillFoots, '_id');

		skillModelInstances.cognitiveSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st', ru: 'Неделя 1', ua: 'Тиждень 1'},
				foots: cognitiveSkillFoots.slice(0,1)
			},
			{
				group_name: {en: 'Week 2nd', ru: 'Неделя 2', ua: 'Тиждень 2'},
				foots: cognitiveSkillFoots.slice(1)
			}
		];

		return skillModelInstances.cognitiveSkill.save();
	})

	// Seeding the angerMediationSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
				name: {ru: 'Определить что такое гнев?'},
				description: {ru: 'Изучить определение гнева. В чему проявляется физиологя гнева? В чем проявляется физиогномика гнева? (ссылки)'},
				practical: false,
				weeks_total: 1,
				tpw: 1,
				dpt: 60,
				order: 0
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
				name: {ru: 'Изучить причины возникновения гнева'},
				description: {ru: 'Понять какие могуть быть ключевые причины базируясь на определении гнева. (список ссылок и литературы прилагается)'},
				practical: false,
				weeks_total: 1,
				tpw: 1,
				dpt: 60,
				order: 1
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
				name: {ru: 'Упражнения по контролю гнева'},
				description: {ru: 'я - сообщение, практика методики 6 секунд, учится безопасно выражать чувства и эмоции'},
				practical: true,
				weeks_total: 2,
				tpw: 3,
				dpt: 20,
				order: 2
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
				name: {ru: 'Изучение теории о личных границах'},
				description: {ru: 'Ознакомится и познать свои личные границы. (список ссылок на тему "личные границы")'},
				practical: false,
				weeks_total: 1,
				tpw: 1,
				dpt: 120,
				order: 3
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
				name: {ru: 'Упражнения'},
				description: {ru: 'Построение "Карты жизни", Упраженения на тему "Да" и "Нет", проведение диганостики личных границ'},
				practical: true,
				weeks_total: 2,
				tpw: 1,
				dpt: 75,
				order: 4
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
				name: {ru: 'Консультация со специалистом по требованию'},
				description: {ru: 'Провести консультацию с профессиональным психологом, если все еще возникает чувство некотрольного гнева'},
				practical: true,
				weeks_total: 1,
				tpw: 1,
				dpt: 60,
				order: 5
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
				name: {ru: 'Изучение последтсвий нарушений личных границ'},
				description: {ru: 'Эскалация гнева в зависимости от критичности нарушения личных границ'},
				practical: false,
				weeks_total: 2,
				tpw: 1,
				dpt: 120,
				order: 6
			}).save()
		]);
	})

	// Creating foot groups for the angerMediationSkill with their foots assigned
	// We also add this skill to user as being studied
	.then(function(angerMediationSkillFoots)
	{

		footModelInstancesWeekFirst = footModelInstancesWeekFirst.concat(angerMediationSkillFoots.slice(1));

		angerMediationSkillFoots = _.pluck(angerMediationSkillFoots, '_id');

		skillModelInstances.angerMediationSkill.foot_groups = [
			{
				group_name: {ru: 'Неделя 1'},
				foots: angerMediationSkillFoots.slice(1)
			}
		];

		userModelInstance.skills.push({
			skill_id: new mongoose.Types.ObjectId(skillModelInstances.angerMediationSkill._id),
			name: skillModelInstances.angerMediationSkill.name.ru,
			image: skillModelInstances.angerMediationSkill.image,
			language: 'ru',
			foots_completed: [ new mongoose.Types.ObjectId(angerMediationSkillFoots[0]) ],
			signed_at: new Date()
		});

		return Promise.all([
			skillModelInstances.angerMediationSkill.save(),
			userModelInstance.save()
		]);

	})

	// Seeding the willpowerImprovementSkill foots...
	.then(function()
	{
		return Promise.all([
			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.willpowerImprovementSkill._id),
				name: {en: 'Kelly McGonigal book', ru: 'Книга Келли Макгонигал'},
				description: {en: 'Read a book of Kelly McGonigal "The Willpower Instinct"', ru: 'Прочитать книгу Келли Макгонигал "Как развить и укрепить силу воли"'},
				practical: false,
				weeks_total: 10,
				tpw: 7,
				dpt: 30,
				order: 0
			}).save(),

			new FootModel({
				skill_id: new mongoose.Types.ObjectId(skillModelInstances.willpowerImprovementSkill._id),
				name: {en: 'Practice on willpower improvement, chapter 1', ru: 'Практика по контролю силы воли, глава 1'},
				description: {en: 'Loren ipsum dolor sit amet', ru: 'Loren ipsum dolor sit amet'},
				practical: true,
				weeks_total: 1,
				tpw: 7,
				dpt: 20,
				order: 1
			}).save()
		])
	})

	// Creating foot groups for the cognitiveSkill with their foots assigned
	.then(function(willpowerImprovementSkillFoots)
	{
		willpowerImprovementSkillFoots = _.pluck(willpowerImprovementSkillFoots, '_id');

		skillModelInstances.cognitiveSkill.foot_groups = [
			{
				group_name: {en: 'Week 1st', ru: 'Неделя 1'},
				foots: willpowerImprovementSkillFoots
			}
		];

		return skillModelInstances.willpowerImprovementSkill.save();
	})

	// adding items to user's organizer
	.then(function()
	{
		var skillIdToLanguageMap = {};
		var createAction = function(foot, index, isSecondWeek)
		{
			var footLanguage = skillIdToLanguageMap[foot.skill_id],
					actionName = foot.name[footLanguage],
					daysOfWeek = [], randomnumber, found
			;

			// picking random N (foot.tpw) day numbers of the week
			while(daysOfWeek.length < foot.tpw)
			{
				randomnumber = Math.ceil(Math.random()*6);
				found = false;
				for(var i = 0; i < daysOfWeek.length; i++)
				{
					if(daysOfWeek[i] === randomnumber)
					{
						found = true;
						break;
					}
				}
				if (isSecondWeek) randomnumber += 7;
				if (!found) daysOfWeek[daysOfWeek.length] = randomnumber;
			}

			_.range(0, foot.tpw).forEach(function(i)
			{
				var organizerAction,
						date = moment().hours(9 + index).minutes(0).seconds(0).weekday(daysOfWeek[i])
				;

				organizerAction = {
					foot_id: new mongoose.Types.ObjectId(foot._id),
					skill_id: new mongoose.Types.ObjectId(foot.skill_id),
					name: actionName,
					dpt: foot.dpt,
					date: date.toDate()
				};
				userModelInstance.organizer_actions.push(organizerAction);
			});
		};

		userModelInstance.skills.forEach(function(skill)
		{
			skillIdToLanguageMap[skill.skill_id] = skill.language;
		});

		_.shuffle(footModelInstancesWeekFirst).slice(0,5).forEach(function(foot, index)
		{
			createAction(foot.toObject(), index, false);
		});

		_.shuffle(footModelInstancesWeekSecond).slice(0,5).forEach(function(foot, index)
		{
			createAction(foot.toObject(), index, true);
		});

		return userModelInstance.save();
	})

	.then(function()
	{
		log('seeding is successfully done');
	})

	.catch(function(err)
	{
		return error(err);
	});
