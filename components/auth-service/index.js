'use strict';

var i18n = require('i18n');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
var validateJwt = expressJwt({secret: process.env.SESSION_SECRET });

module.exports = {

	/**
	 * Attaches the user object to the request if authenticated
	 * Otherwise returns 403
	 */
	isAuthenticated: function ()
	{
		return compose()

			// Validate jwt
			.use(function (req, res, next)
			{
				if (req.query && req.query.hasOwnProperty('access_token'))
				{
					req.headers.authorization = 'Bearer ' + req.query.access_token;
				}
				return validateJwt(req, res, next);
			})

			// Handling errors if any
			.use(function(err, req, res, next)
			{
				if (err)
				{
					if (err.message === 'jwt expired')
					{
						err.name = '';
						err.message = 'Authorization expired, please re-enter your credentials';
					}
					return res.status(401).json({error: err.message}).end();
				}
				return next(err || null);
			})

			// If passed - setting locale
			.use(function(req, res, next)
			{
				req.setLocale(req.user.language);
				next(null);
			});
	},

	/**
	 * Checks if the user role meets the minimum requirements of the route
	 * @param roleRequired
	 */
	hasRole: function (roleRequired)
	{
		if (!roleRequired) throw new Error('Required role needs to be set');

		return compose()
			.use(this.isAuthenticated())
			.use(function meetsRequirements(req, res, next)
			{
				var userRoles = ['user', 'manager', 'admin'];
				if (userRoles.indexOf(req.user.role) >= userRoles.indexOf(roleRequired))
				{
					return next(null);
				}
				else
				{
					return res.status(403).end();
				}
			});
	},

	/**
	 * Returns a jwt token signed by the app secret
	 * @param id
	 * @param role
	 */
	signToken: function (id, role, language)
	{
		return jwt.sign({ id: id, role: role, language: language }, process.env.SESSION_SECRET, {
			expiresInMinutes: 60 * 24 * 120	//120 days
		});
	},

	/**
	 * Verifies the returned previously token - needed only for unit tests
	 * @param token
	 * @param callback
	 */
	verifyToken: function(token, callback)
	{
		return jwt.verify(token, process.env.SESSION_SECRET, {}, callback);
	}

};
